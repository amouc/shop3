package com.tom.shop.data

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.tom.shop.Article

@Dao
interface ArticleDao {
    @Query("select * from articles")
    fun getAll() : List<Article>

    @Query("select * from articles where id = :id")
    fun getArticleById(id: Long): Article

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun add(article: Article)
}