package com.tom.shop.data

import androidx.room.Database
import androidx.room.RoomDatabase
import com.tom.shop.Article

@Database(entities = [Article::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun articleDao() : ArticleDao
}