package com.tom.shop

import org.jsoup.Jsoup

fun main() {
    Jsoup.connect("https://www.starbucks.com.tw/products/drinks/view.jspx?catId=1")
        .get()
        .run{
            select(".list_expresso div")
                .forEachIndexed { index, element ->
                    val name = element.select("h4")[0].text()
                    val imageUrl = element.select("img")
                        .attr("src")
                    println("$name / $imageUrl")
                }
        }

}