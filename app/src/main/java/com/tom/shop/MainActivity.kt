package com.tom.shop

import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import android.view.*
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity;
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.tom.shop.model.Drink
import com.tom.shop.view.DrinkAdapter

import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*
import org.jetbrains.anko.*
import org.json.JSONArray
import java.net.URL

class MainActivity : AppCompatActivity(), DrinkAdapter.OnDrinkClickedListener, AnkoLogger {
    val TAG = MainActivity::class.java.simpleName

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
        }
        //Fruits
        val fruits = listOf<String>("Apple", "Banana", "Pineapple")

        recycler.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(this@MainActivity)
        }
        val drinks = listOf<Drink>(
            Drink("Latte", 115, 300, null),
            Drink("Espreso", 95, 260, null),
            Drink("Moca", 130, 345, null)
        )
        longToast("Hello")
//        alert { okButton {  } }
        doAsync {
            val json = URL("https://api.myjson.com/bins/bh9ly")
                .readText()
            info(json)
            val drinks = Gson().fromJson<List<Drink>>(json,
                object : TypeToken<List<Drink>>(){}.type )
            uiThread {
                val adapter = DrinkAdapter(drinks)
                recycler.adapter = adapter
                adapter.onDrinkClickedListener = this@MainActivity
            }
        }
//        DrinkTask().execute("https://api.myjson.com/bins/bh9ly")
    }

    inner class DrinkTask : AsyncTask<String, Void, String>() {
        val TAG = DrinkTask::class.java.simpleName

        override fun doInBackground(vararg params: String?): String {
            val json = URL(params[0])
                .readText()
            Log.d(TAG, "json: $json");
            return json
        }

        override fun onPostExecute(result: String?) {
            Log.d(TAG, "onPostExecute: $result");
            val array = JSONArray(result)
            val drinks = mutableListOf<Drink>()
            for (n in 0..array.length()-1) {
                val obj = array.getJSONObject(n)
                val name = obj.getString("name")
                val price = obj.getInt("price")
                Log.d(TAG, "$name  $price");
                val drink = Drink(name, price, 0, null)
                drinks.add(drink)
            }
            val adapter = DrinkAdapter(drinks)
            recycler.adapter = adapter
            adapter.onDrinkClickedListener = this@MainActivity
        }
    }

    inner class FruitAdapter(val fruits: List<String>) : RecyclerView.Adapter<FruitViewHolder>(){
        override fun onCreateViewHolder(
            parent: ViewGroup, viewType: Int): FruitViewHolder {
            val view = LayoutInflater.from(parent.context)
                .inflate(android.R.layout.simple_list_item_1, parent, false)
            return FruitViewHolder(view)
        }

        override fun getItemCount(): Int {
            return fruits.size
        }

        override fun onBindViewHolder(holder: FruitViewHolder, position: Int) {
            holder.nameText.text = fruits.get(position)
            holder.itemView.setOnClickListener {
                onFruitClicked(fruits.get(position))
            }
        }
    }

    private fun onFruitClicked(fruitName: String) {

    }

    class FruitViewHolder(view: View) :
            RecyclerView.ViewHolder(view) {
        var nameText: TextView =
            view.findViewById(android.R.id.text1)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            R.id.action_camera -> {
                startActivity<CameraActivity>(
                    "COUNTER" to 3,
                    "LEVEL" to  7,
                    "USERID" to "hank")
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun drinkClicked(drink: Drink, position: Int) {
        Log.d(TAG, "drinkClicked: ${drink.name} / $position")
        if (position == 0) {
            startActivity(Intent(this, NewsActivity::class.java))
        }
    }
}
