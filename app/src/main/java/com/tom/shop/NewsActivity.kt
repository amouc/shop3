package com.tom.shop

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.room.*
import com.google.gson.Gson
import com.tom.shop.data.AppDatabase
import com.tom.shop.view.ArticleAdapter
import kotlinx.android.synthetic.main.activity_news.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.net.URL

class NewsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_news)
        //java 寫法
        /*val trans = supportFragmentManager.beginTransaction()
        trans.replace(R.id.news_container, NewsFragment())
        trans.commit()*/

        //把Fragment 放進來
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.news_container, NewsFragment())
            commit()
        }
    }
}
data class NewsApi(
    val articles: List<Article>,
    val status: String,
    val totalResults: Int
)

@Entity(tableName = "articles")
data class Article(
    @PrimaryKey(autoGenerate = true)
    var id: Long,
    @ColumnInfo(name= "writer")
    var author: String?,
    var content: String?,
    var description: String,
    var publishedAt: String,
    @Ignore var source: Source?,
    var title: String,
    var url: String,
    var urlToImage: String?
){
    constructor(): this(0,"", "", "", "", Source(0,""), "", "", "")
}

data class Source(
    val id: Any?,
    val name: String
)
