package com.tom.shop

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.room.Room
import com.google.gson.Gson
import com.tom.shop.data.AppDatabase
import com.tom.shop.view.ArticleAdapter
import kotlinx.android.synthetic.main.fragment_new.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.net.URL

class NewsFragment : Fragment(){
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.fragment_new,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        //生命週期相關的 super 不要刪
        super.onViewCreated(view, savedInstanceState)
        recycler.apply {
            setHasFixedSize(true)
            layoutManager =
                LinearLayoutManager(context)
            doAsync {
                val json = URL("https://newsapi.org/v2/top-headlines?country=tw&category=sports&apiKey=598a146603914c189658850797bf5caa")
                    .readText()
                val newsApi =
                    Gson().fromJson<NewsApi>(json, NewsApi::class.java)

                uiThread {
                    it.adapter = ArticleAdapter(newsApi.articles)
                }
                /*for (i in 0..newsApi.articles.size-1) {
                    val article = newsApi.articles.get(i)
                }*/
                val database = Room.databaseBuilder(context,
                    AppDatabase::class.java, "mydb")
                    .build()
                val dao = database.articleDao()
                newsApi.articles.forEachIndexed { i, article ->
                    dao.add(article)
                }
            }
        }
    }
}