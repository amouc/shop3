package com.tom.shop.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.tom.shop.model.Drink
import com.tom.shop.R
import kotlinx.android.synthetic.main.row_drink.view.*

class DrinkAdapter(val drinks: List<Drink>) :
                RecyclerView.Adapter<DrinkAdapter.DrinkHolder>(){
    var onDrinkClickedListener: OnDrinkClickedListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DrinkHolder {
        return DrinkHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.row_drink, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return drinks.size
    }

    override fun onBindViewHolder(holder: DrinkHolder, position: Int) {
        holder.nameText.text = drinks.get(position).name
        holder.priceText.text = drinks.get(position).price.toString()
        holder.itemView.setOnClickListener {
            onDrinkClickedListener?.drinkClicked(drinks.get(position), position)
        }
    }

    class DrinkHolder(view:View) : RecyclerView.ViewHolder(view) {
        val nameText = view.drink_name
        val priceText = view.drink_price
    }

    interface OnDrinkClickedListener {
        fun drinkClicked(drink: Drink, position: Int)
    }

}