package com.tom.shop.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.tom.shop.Article
import com.tom.shop.R
import kotlinx.android.synthetic.main.article_row.view.*

class ArticleAdapter(val articles: List<Article>) :
    RecyclerView.Adapter<ArticleHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):
            ArticleHolder {
        return ArticleHolder(parent.inflate(R.layout.article_row))
    }

    override fun getItemCount(): Int {
        return articles.size
    }

    override fun onBindViewHolder(holder: ArticleHolder, position: Int) {
        val article = articles.get(position)
        holder.titleText.text = article.title
        Glide.with(holder.itemView.context)
            .load(article.urlToImage)
            .override(200, 150)
            .into(holder.image)
    }

}

class ArticleHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val titleText = itemView.article_title
    val image = itemView.article_image
}

fun ViewGroup.inflate(layout: Int) :View {
    val view = LayoutInflater.from(this.context)
        .inflate(layout, this, false)
    return view
}
