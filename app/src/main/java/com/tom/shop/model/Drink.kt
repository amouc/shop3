package com.tom.shop.model

data class Drink(
    val name: String,
    var price: Int,
    var cal: Int,
    var imageUrl: String?)

fun main() {
    val drinks = listOf<Drink>(
        Drink("Latte", 115, 300, null),
        Drink("Espreso", 95, 260, null),
        Drink("Moca", 130, 345, null)
    )
}